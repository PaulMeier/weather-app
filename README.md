# weather-app

npm install

node app.js

Options:
  --version      Show version number                                   [boolean]
  --address, -a  Address to fetch weather for.               [string] [required]
  --help, -h     Show help                                             [boolean]
